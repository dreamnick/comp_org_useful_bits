@ Name: Michael Rogers
@ Date: 19 April, 2019
@ Description: Demo of lr and pc

.func main
.global main

.data

.text

main:
	bl fun
	cmp r0, #0
	
	bx lr

fun:	push {lr}
	mov r0, #10
	pop {lr}
	
