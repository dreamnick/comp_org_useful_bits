@ Program Name: debuggingExercise.s
@ Author: Anonymous
@ Date: 25 February, 2019
@ Description: A program that doesn't work :-(

.global main
.func main

.data
	startingPoint: .word 0
.balign 4
	startingPointMessage: .asciz "Enter starting point for countdown: "	
	scanPattern: .asciz "%d"
	countdownPattern: .asciz "%d\n"
  
.text
main: push {lr}                 @ preserve lr
       
	ldr r0, =startingPointMessage
	bl puts			@ puts("Enter starting point for countdown: ")
	
	ldr r0, =scanPattern    @ r0 <- address of scanPattern
	ldr r1, =startingPoint  @ r1 <- address of startingPoint
	bl scanf		@ scanf("%d", &startingPoint)
	
	ldr r2, =startingPoint  @ r2 <- address of startingPoint
	ldr r2, [r2]		@ r2 <- startingPoint

nextWhile:
	cmp r2, #0		@ are we down to 0?
	beq endWhile            @ if we hit 0, leave the while loop

	ldr r0, =countdownPattern
 	mov r1, r2		@ in preparation for printf("%d\n", i)
	bl printf
  	add r2, r2, #-1		@ r2--
	b nextWhile             @ back to the top of the loop

endWhile:		
 
	pop {lr}
	bx lr 
