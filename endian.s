@ Program Name: endian.s
@ Author: M.P. Rogers
@ Date: 12 February, 2019
@ Description: For use in a query about endian-ism

.global main
.func main

.data
.balign 4
x:   .word 0xf5a7cc35
y:   .word 200

.text
main:
        ldr r1, =x
        ldr r1,[r1]
        ldr r2, =y
        ldr r2, [r2]
        add r0, r1, r2
        bx lr