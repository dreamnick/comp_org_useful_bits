# endian

This is a demo to show the endian-nature (big endian v. little endian) of a Raspberry Pi.

Once you have downloaded it, perform these steps:

as -g -o endian.o endian.s
gcc -o endian endian.o
gdb endian

